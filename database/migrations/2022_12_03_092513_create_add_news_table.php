<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('add_news', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image');
            $table->unsignedInteger('news_type_id');
            $table->foreign('news_type_id')->references('id')
                ->on('news_types')->onDelete('cascade');
            $table->string('title');
            $table->string('text');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('add_news');
    }
};
