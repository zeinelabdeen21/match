<?php
use App\Http\Controllers\Dashboard\HomeController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Dashboard\Admin\LoginController;
// use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Dashboard\NewsTypeController;
use App\Http\Controllers\Dashboard\TeamController;
use App\Http\Controllers\Dashboard\AddTeamController;
use App\Http\Controllers\Dashboard\AddNewsController;
use App\Http\Controllers\Dashboard\ImportantNewsController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Auth::routes();


// Route::get('/', function () {
//     return view('auth.register');
// });


Route::prefix('admin')->name('admin.')->group(function () {
    Route::get('login',[LoginController::class, 'get']);
    Route::post('login/post',[LoginController::class, 'post']);

});


Route::prefix('dashboard')->name('dashboard.')->middleware('web')->group(function () {
    Route::get('/index',[HomeController::class, 'index'])->name('home');

    Route::prefix('news')->middleware('web')->group(function () {
        Route::get('index',[AddNewsController::class, 'index']);
        Route::get('create',[AddNewsController::class, 'create']);
        Route::post('store',[AddNewsController::class, 'store']);
        Route::get('delete/{id}',[AddNewsController::class, 'destroy']);
        Route::get('edit/{id}',[AddNewsController::class, 'edit']);
        Route::post('update',[AddNewsController::class, 'update']);
    });

    Route::prefix('newsType')->middleware('web')->group(function () {
        Route::get('index',[NewsTypeController::class, 'index']);
        Route::get('create',[NewsTypeController::class, 'create']);
        Route::post('store',[NewsTypeController::class, 'store']);
        Route::get('delete/{id}',[NewsTypeController::class, 'destroy']);
        Route::get('edit/{id}',[NewsTypeController::class, 'edit']);
        Route::post('update',[NewsTypeController::class, 'update']);
    });

    Route::prefix('team')->middleware('web')->group(function () {
        Route::get('index',[TeamController::class, 'index']);
        Route::get('create',[TeamController::class, 'create']);
        Route::post('store',[TeamController::class, 'store']);
        Route::get('delete/{id}',[TeamController::class, 'destroy']);
        Route::get('edit/{id}',[TeamController::class, 'edit']);
        Route::post('update',[TeamController::class, 'update']);
    });

    Route::prefix('addTeam')->middleware('web')->group(function () {
        Route::get('index',[AddTeamController::class, 'index']);
        Route::get('create',[AddTeamController::class, 'create']);
        Route::post('store',[AddTeamController::class, 'store']);
        Route::get('delete/{id}',[AddTeamController::class, 'delete']);
        Route::get('edit/{id}',[AddTeamController::class, 'edit']);
        Route::post('update',[AddTeamController::class, 'update']);
    });
    // أهم الاخبار================================================================================================
    Route::prefix('importantNews')->middleware('web')->group(function () {
        Route::get('index',[ImportantNewsController::class, 'index']);
        Route::get('create',[ImportantNewsController::class, 'create']);
        Route::post('store',[ImportantNewsController::class, 'store']);
        Route::get('delete/{id}',[ImportantNewsController::class, 'destroy']);
        Route::get('edit/{id}',[ImportantNewsController::class, 'edit']);
        Route::post('update',[ImportantNewsController::class, 'update']);
    });

});



// Route::get('/', function () {
//     return view('welcome');
// });

// php artisan make:controller Dashboard/Admin/LoginController

// php artisan make:controller Dashboard/ImportantNewsController --model=ImportantNews

// php artisan make:controller Dashboard/Admin/LoginController --model=User
// php artisan make:model ImportantNews --migration
// php artisan migrate
// php artisan refresh
// news

// Auth::routes();


// Route::get('/', function () {
//     return view('auth.login');
// });


// Route::get('/{page}', [AdminController::class, 'index']);

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// Members
