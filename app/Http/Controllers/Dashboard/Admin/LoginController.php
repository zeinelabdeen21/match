<?php

namespace App\Http\Controllers\Dashboard\Admin;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function get() {
        return view('auth.login');
    }

    public function post() {
//       dd(request()->all());
        $this->validate(request(),[
            'email' => 'required|email|max:225',
            'password' => 'required|string|min:8|max:35',
        ]);


        $remember = request()->has('remember')? true:false;
        $credentials = array('email' => request()->email, 'password' => request()->password);
        $checkLogin = Auth::guard('web')->attempt($credentials,$remember);
        if (!$checkLogin){
            session()->flash('message','البيانات غير صحيحة');
            return redirect('auth/login');

        }
        return redirect('/dashboard/index');


    }

    public function logout () {
        auth()->guard('admin')->logout();
        return redirect('/auth/login');
    }
}
