<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\AddTeam;
use App\Models\Team;
use Illuminate\Http\Request;
use Validator;

class AddTeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data = AddTeam::get();
        return view('addTeam.index', compact('data'));
    }

    /**
     * Show the form for creating a AddTeam resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $teams = Team::orderBy('id','desc')->get();

        return view('addTeam.create',compact('teams'));

               // return view('addTeam.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //       dd($request->all());


        Validator::make(
            request()->all(),
            [
                'title'  => 'required|string',
                'text'  => 'required|string',
                'team' => 'required|integer|exists:teams,id',
                'image'      => 'required|image|mimes:jpg,jpeg,png|max:5120',

            ]
        )->validate();

        $imageName = md5(time()) . '.' . request()->file('image')->getClientOriginalExtension();
        $imageMove = request()->file('image')->move(public_path('uploads/addTime/'), $imageName);

        $photoUrl = url('uploads/addTime/' . $imageName);
        if (!$photoUrl) {
            return response()->json([
                'status' => false,
                'message' => 'حدث شئ ما خطأ لم يتم رفع الصورة',
            ], 200);
        }

        $create = AddTeam::create([

            'title'       => request()->title,
            'text'       => request()->text,
            'image'       => $photoUrl,
            'team_id'   => request()->team,

        ]);
        if (!$create) {
            return back()->with('error', trans('response.failed'));
        }
        return back()->with('success', trans('response.added'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AddTeam  $addTeam
     * @return \Illuminate\Http\Response
     */
    public function show(AddTeam $addTeam)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AddTeam  $addTeam
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $this->validate(request(), ['id' => $id], [
        //     'id'  => 'required|integer|exists:add_teams,id',
        // ]);
        // $data = AddTeam::findOrFail($id);
        // return view('addTime.edit', compact('data'));

        Validator::make(
            ['id' => $id],
            ['id' => 'required|integer|exists:add_teams,id'],
            [])->validate();
        $data = AddTeam::findOrFail($id);
        $teams = Team::orderBy('id','desc')->get();

        return view('addTeam.edit',compact('data','teams'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\AddTime  $addTime
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AddTeam $addTime)
    {
        Validator::make(
            request()->all(),
            [
                'id'    => 'required|integer|exists:add_teams,id',
                'title'  => 'nullable|string',
                'text'  => 'nullable|string',
                'team' => 'nullable|integer|exists:teams,id',
                'image'      => 'nullable|image|mimes:jpg,jpeg,png|max:5120',

            ]
        )->validate();
        $addTime = AddTeam::findOrFail($request->id);

        if ($request->hasFile('image')) {
            $imageName = md5(time()) . '.' . request()->file('image')->getClientOriginalExtension();
            $imageMove = request()->file('image')->move(public_path('uploads/addTime'), $imageName);
            $photoUrl = url('uploads/addTime/' . $imageName);
            if (!$photoUrl) {
                return response()->json(['message' => trans('response.failed')], 444);
            }
            if ($addTime->image != null && file_exists(public_path('uploads/addTime/' . $addTime->image))) {
                unlink(public_path('uploads/addTime/' . $addTime->image));
            }
            $inputs['image'] = $photoUrl;
        }
        $addTime->title = (request()->title == null)? $addTime->title :request()->title;
        $addTime->title = (request()->text == null)? $addTime->title :request()->text;
        $addTime->team_id = (request()->team == null)? $addTime->team_id :request()->team;
        $addTime->image = (request()->image == null)? $addTime->image : $photoUrl;

        $update = $addTime->save();
        if (!$update) {
            return back()->with('error', trans('response.failed'));
        }
        return back()->with('success', trans('response.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AddTime  $addTime
     * @return \Illuminate\Http\Response
     */

    public function delete($id)
    {
        Validator::make(
            [
                'id' => $id,
            ],
            [
                'id' => 'required|integer|exists:add_teams,id',
            ]
        )->validate();
        $new = AddTeam::findOrFail(request()->id);
        $delete = $new->delete();
        if (!$delete) {
            return back()->with('error', trans('response.failed'));
        }
        if ($new->image != null && file_exists(public_path('uploads/addTime/' . $new->image))) {
            unlink(public_path('uploads/addTime/' . $new->image));
        }
        return back()->with('success', trans('response.deleted'));
    }
}
