<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\AddNews;
use App\Models\NewsType;
use Illuminate\Http\Request;
use Validator;

class AddNewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = AddNews::get();
        return view('news.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $teams = NewsType::orderBy('id','desc')->get();
        return view('news.create',compact('teams'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        Validator::make(
            request()->all(),
            [
                'title'  => 'required|string',
                'text'  => 'required|string',
                'addNews' => 'required|integer|exists:news_types,id',
                'image'      => 'required|image|mimes:jpg,jpeg,png|max:5120',

            ]
        )->validate();

        $imageName = md5(time()) . '.' . request()->file('image')->getClientOriginalExtension();
        $imageMove = request()->file('image')->move(public_path('uploads/news/'), $imageName);

        $photoUrl = url('uploads/news/' . $imageName);
        if (!$photoUrl) {
            return response()->json([
                'status' => false,
                'message' => 'حدث شئ ما خطأ لم يتم رفع الصورة',
            ], 200);
        }

        $create = AddNews::create([

            'title'       => request()->title,
            'text'       => request()->text,
            'image'       => $photoUrl,
            'news_type_id'   => request()->addNews,

        ]);
        if (!$create) {
            return back()->with('error', trans('response.failed'));
        }
        return back()->with('success', trans('response.added'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AddNews  $addNews
     * @return \Illuminate\Http\Response
     */
    public function show(AddNews $addNews)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AddNews  $addNews
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Validator::make(
            ['id' => $id],
            ['id' => 'required|integer|exists:add_news,id'],
            [])->validate();
        $data = AddNews::findOrFail($id);
        $teams = NewsType::orderBy('id','desc')->get();

        return view('news.edit',compact('data','teams'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\AddNews  $addNews
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AddNews $addNews)
    {
        Validator::make(
            request()->all(),
            [
                'id'    => 'required|integer|exists:add_news,id',
                'title'  => 'nullable|string',
                'text'  => 'nullable|string',
                'news_type' => 'nullable|integer|exists:news_types,id',
                'image'      => 'nullable|image|mimes:jpg,jpeg,png|max:5120',

            ]
        )->validate();
        $addNews = AddNews::findOrFail($request->id);

        if ($request->hasFile('image')) {
            $imageName = md5(time()) . '.' . request()->file('image')->getClientOriginalExtension();
            $imageMove = request()->file('image')->move(public_path('uploads/addNews'), $imageName);
            $photoUrl = url('uploads/news/' . $imageName);
            if (!$photoUrl) {
                return response()->json(['message' => trans('response.failed')], 444);
            }
            if ($addNews->image != null && file_exists(public_path('uploads/addNews/' . $addNews->image))) {
                unlink(public_path('uploads/news/' . $addNews->image));
            }
            $inputs['image'] = $photoUrl;
        }
        $addNews->title = (request()->title == null)? $addNews->title :request()->title;
        $addNews->text = (request()->text == null)? $addNews->title :request()->text;
        $addNews->team_id = (request()->team == null)? $addNews->team_id :request()->team;
        $addNews->image = (request()->image == null)? $addNews->image : $photoUrl;

        $update = $addNews->save();
        if (!$update) {
            return back()->with('error', trans('response.failed'));
        }
        return back()->with('success', trans('response.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AddNews  $addNews
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Validator::make(
            [
                'id' => $id,
            ],
            [
                'id' => 'required|integer|exists:news_types,id',
            ]
        )->validate();
        $new = AddNews::findOrFail(request()->id);
        $delete = $new->delete();
        if (!$delete) {
            return back()->with('error', trans('response.failed'));
        }
        if ($new->image != null && file_exists(public_path('uploads/news/' . $new->image))) {
            unlink(public_path('uploads/news/' . $new->image));
        }
        return back()->with('success', trans('response.deleted'));
    }
}
