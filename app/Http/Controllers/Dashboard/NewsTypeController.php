<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\NewsType;
use Illuminate\Http\Request;
use Validator;

class NewsTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = NewsType::get();
        return view('newsType.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $teams = NewsType::orderBy('id','desc')->get();

        return view('newsType.create',compact('teams'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title'  => 'required|string',
        ]);



        $inputs['title'] = $request->title;


        $create = NewsType::create($inputs);
        if (!$create) {
            return back()->with('error',trans('response.failed'));
        }
        return back()->with('success',trans('response.added'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\NewsType  $newsType
     * @return \Illuminate\Http\Response
     */
    public function show(NewsType $newsType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\NewsType  $newsType
     * @return \Illuminate\Http\Response
     */
    public function edit(NewsType $newsType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\NewsType  $newsType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, NewsType $newsType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\NewsType  $newsType
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Validator::make(
            [
                'id' => $id,
            ],
            [
                'id' => 'required|integer|exists:news_types,id',
            ])->validate();
        $team = NewsType::find($id);
        if (!$team->delete()) {
            return back()->with('error',trans('response.failed'));
        }
        return back()->with('success',trans('response.deleted'));
    }
}
