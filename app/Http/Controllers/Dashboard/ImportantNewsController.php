<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\ImportantNews;
use Illuminate\Http\Request;
use Validator;
class ImportantNewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = ImportantNews::get();
        return view('importantNews.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('importantNews.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make(
            request()->all(),
            [
                'title'  => 'required|string',
                'text'  => 'required|string',
                'image'      => 'required|image|mimes:jpg,jpeg,png|max:5120',

            ]
        )->validate();

        $imageName = md5(time()) . '.' . request()->file('image')->getClientOriginalExtension();
        $imageMove = request()->file('image')->move(public_path('uploads/importantNews/'), $imageName);

        $photoUrl = url('uploads/importantNews/' . $imageName);
        if (!$photoUrl) {
            return response()->json([
                'status' => false,
                'message' => 'حدث شئ ما خطأ لم يتم رفع الصورة',
            ], 200);
        }

        $create = ImportantNews::create([
            'title'       => request()->title,
            'text'       => request()->text,
            'image'       => $photoUrl,
        ]);
        if (!$create) {
            return back()->with('error', trans('response.failed'));
        }
        return back()->with('success', trans('response.added'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ImportantNews  $importantNews
     * @return \Illuminate\Http\Response
     */
    public function show(ImportantNews $importantNews)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ImportantNews  $importantNews
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Validator::make(
            ['id' => $id],
            ['id' => 'required|integer|exists:important_news,id'],
            [])->validate();
        $data = ImportantNews::findOrFail($id);

        return view('importantNews.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ImportantNews  $importantNews
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ImportantNews $importantNews)
    {
        Validator::make(
            request()->all(),
            [
                'id'    => 'required|integer|exists:important_news,id',
                'title'  => 'nullable|string',
                'text'  => 'nullable|string',
                'image'      => 'nullable|image|mimes:jpg,jpeg,png|max:5120',

            ]
        )->validate();
        $importantNews = ImportantNews::findOrFail($request->id);

        if ($request->hasFile('image')) {
            $imageName = md5(time()) . '.' . request()->file('image')->getClientOriginalExtension();
            $imageMove = request()->file('image')->move(public_path('uploads/importantNews'), $imageName);
            $photoUrl = url('uploads/importantNews/' . $imageName);
            if (!$photoUrl) {
                return response()->json(['message' => trans('response.failed')], 444);
            }
            if ($importantNews->image != null && file_exists(public_path('uploads/importantNews/' . $importantNews->image))) {
                unlink(public_path('uploads/importantNews/' . $importantNews->image));
            }
            $inputs['image'] = $photoUrl;
        }
        $importantNews->title = (request()->title == null)? $importantNews->title :request()->title;
        $importantNews->text = (request()->text == null)? $importantNews->title :request()->text;
        $importantNews->image = (request()->image == null)? $importantNews->image : $photoUrl;

        $update = $importantNews->save();
        if (!$update) {
            return back()->with('error', trans('response.failed'));
        }
        return back()->with('success', trans('response.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ImportantNews  $importantNews
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Validator::make(
            [
                'id' => $id,
            ],
            [
                'id' => 'required|integer|exists:important_news,id',
            ]
        )->validate();
        $ImportantNews = ImportantNews::findOrFail(request()->id);
        $delete = $ImportantNews->delete();
        if (!$delete) {
            return back()->with('error', trans('response.failed'));
        }
        if ($ImportantNews->image != null && file_exists(public_path('uploads/importantNews/' . $ImportantNews->image))) {
            unlink(public_path('uploads/importantNews/' . $ImportantNews->image));
        }
        return back()->with('success', trans('response.deleted'));
    }
}
