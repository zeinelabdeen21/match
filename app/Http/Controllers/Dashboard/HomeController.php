<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\AddNews;
use App\Models\AddTeam;
use App\Models\Team;
use Illuminate\Http\Request;
use App\Models\User;
class HomeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // return view('index');
        $data['addTeam'] = AddTeam::get()->count();
        $data['team'] = Team::get()->count();
        $data['news'] = AddNews::get()->count();

        return view('index',compact('data'));
    }
    // public function index () {
    //     // users
    //     $data['users'] = User::get()->count();
    //     return view('index',compact('data'));
    // }

    // public function index($id)
    // {
    //     if(view()->exists($id)){
    //         return view($id);
    //     }
    //     else
    //     {
    //         return view('404');
    //     }

    //  //   return view($id);
    // }
}
